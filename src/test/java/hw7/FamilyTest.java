package hw7;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    public HashMap<String, String> schedule(){
         HashMap<String, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.SUNDAY.name(),"do home work");
        schedule.put(DayOfWeek.MONDAY.name(),"go to courses; watch a film");
        schedule.put(DayOfWeek.TUESDAY.name(),"Learn React");
        schedule.put(DayOfWeek.WEDNESDAY.name(),"Learn JS");
        schedule.put(DayOfWeek.THURSDAY.name(),"Go to the park");
        schedule.put(DayOfWeek.FRIDAY.name(),"Buy a coffee cup");
        schedule.put(DayOfWeek.SATURDAY.name(),"Learn japanese");
        return schedule;
    }

    public Family createFamily(){
        HashSet<String> habits = new HashSet<>();
        habits.add("eat meat");
        HashSet<Pet> pets = new HashSet<>();
        pets.add(new Dog("mukhtar",5,10, habits));

        ArrayList<Human> children = new ArrayList<>();
        children.add(new Man("Sergey", "Liskov", 10,120, schedule()));
        children.add(new Woman("Elena", "Liskova", 3,60, schedule()));

        Family f1 = new Family(
                new Woman("Yoneko", "Patterson", 60, 90, schedule()),
                new Man("Vanya", "Patterson", 53, 90, schedule()),
                children,
                pets);
        return f1;
    }

    @Test
    void addChild() {
        Family f1 = createFamily();
        Woman c = new Woman("TEST1", "TEST2", 10, 90, schedule());
        f1.addChild(c);
        assertEquals(f1.getChildren().contains(c), true);
    }

    @Test
    void deleteChild() {
        Family f1 = createFamily();
        Woman c = new Woman("Elena", "Liskova", 3, 60, schedule());
        f1.deleteChild(c);
        assertFalse(f1.getChildren().contains(c));
        c.setName("Marina");
        f1.deleteChild(c);
        assertNotEquals(f1.getChildren().contains(c), true);
        f1.deleteChild(0);
        assertEquals(f1.getChildren().size(), 0);
    }


    @Test
    void count() {
        Family f1 = createFamily();

        int real = 4;
        int expect = f1.count();
        assertEquals(expect,real);
    }
}