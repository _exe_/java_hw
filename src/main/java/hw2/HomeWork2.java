package hw2;

import java.util.Scanner;
import java.util.StringJoiner;
import java.util.function.Predicate;

public class HomeWork2 {
    static final int AREA_COUNT = 5;
    static String target;
    static int targetY = randomInteger();
    static int targetX = randomInteger();


    public static void main(String[] args) {
        start();
    }

    public static int randomInteger(){
        int randomNum = (int) ((Math.random() * AREA_COUNT));
        return randomNum;
    }

    public static int expectValue(Scanner sc) {
        do {
            String input = sc.next();
            int value = 0;
            if (isInt(input)){
                value = toInt(input);
            } else {
                System.out.printf("You have not typed an number %s\n", input);
            }
            if (value >= 1 && value <= AREA_COUNT){
                return value - 1;
            } else {
                System.out.printf("You must type a number between 1 and %d. Your current number is %s\n", AREA_COUNT, input);
            }
        }while (true);
    }

    public static int toInt(String input) {
        return Integer.parseInt(input);
    }

    public static boolean isInt(String input) {
        try {
            int value = toInt(input);
           return true;
        } catch (NumberFormatException x) {
            return false;
        }
    }

    public static int userNumber(){
        Scanner sc = new Scanner(System.in);
        int userNumber = expectValue(sc);
            return userNumber;
    }

    private static void start() {
        String[][] area = new String[AREA_COUNT][AREA_COUNT];
        System.out.printf("y %d, x %d \n", targetY+1,targetX +1);
        System.out.println("All set. Get ready to rumble!");
        game(area);
    }
    
    public static void print(String[][] area){
        for (int y = 0; y< area.length; y++){
                System.out.println(printCols(area[y], x -> x == null ));
        }
    }
    
    public static String printCols(String[] a, Predicate<String> predicate) {
        StringJoiner sj = new StringJoiner(" | ");
        for (String x: a) {
            sj.add(predicate.test(x) ? String.format("-") :String.format("%s", x));
        }
        return sj.toString();
    }

    public static void game(String[][] area) {
        boolean isMatch = false;
        do{
            int y = userNumber();
            int x = userNumber();
            if (targetY == y && targetX == x){
                isMatch = true;
                area[targetY][targetX]= "x";
                print(area);
                System.out.println("You win!");
            } else{
                area[y][x] = "*";
                print(area);

            }
        }while (!isMatch);
    }
}
