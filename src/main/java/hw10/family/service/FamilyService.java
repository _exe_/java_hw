package hw10.family.service;


import hw10.*;
import hw10.family.collection.CollectionFamilyDao;
import hw10.family.dao.FamilyDao;

import java.time.LocalDate;
import java.time.Period;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FamilyService {
    private FamilyDao<Family> db  = new CollectionFamilyDao<>();

    public List<Family> geAllFamilies(){
        return db.getAllFamilies();
    }

    public void displayAllFamilies(){
        List<Family> allFamilies = db.getAllFamilies();
        allFamilies.forEach(x-> System.out.printf("Index: %d Family: %s", allFamilies.indexOf(x), x));

    }

    public void displayAllFamilies(List<Family> families) {
        families.forEach(x-> System.out.printf("Index: %d Family: %s", families.indexOf(x), x));
    }

    public void getFamiliesBiggerThan(int c){
        List<Family> collect = db.getAllFamilies().stream()
                .filter(x -> x.count() > c)
                .collect(Collectors.toList());
         displayAllFamilies(collect);
    }

    public void getFamiliesLessThan(int c){
        List<Family> collect = db.getAllFamilies().stream()
                .filter(x -> x.count() < c)
                .collect(Collectors.toList());
        displayAllFamilies(collect);
    }

    public void countFamiliesWithMemberNumber(int c){
        long count = db.getAllFamilies().stream().filter(x -> x.count() == c).count();
        System.out.printf("countFamiliesWithMemberNumber: %d \n",count);

    }

    public void createNewFamily(Woman mother, Man father){
        Family family = new Family(mother, father);
        db.saveFamily(family);
    }

    public void deleteFamilyByIndex(int index){
        System.out.println(db.deleteFamily(index));
    }

    public void bornChild(Family f, String maleName, String femaleName){
        if (db.getAllFamilies().contains(f)){
            int randomGender = (int) (Math.random()*1);
            Human ch;
            if (randomGender == 1){
                ch = new Woman(femaleName, f.getMother().getSurname());
            } else {
                ch = new Man(maleName, f.getMother().getSurname());
            }
            f.addChild(ch);
            int i = db.getAllFamilies().indexOf(f);
            db.getAllFamilies().set(i,f);
        }
    }

    public void adoptChild(Family f, Human child){
        if (db.getAllFamilies().contains(f)){
            f.addChild(child);
            int i = db.getAllFamilies().indexOf(f);
            db.getAllFamilies().set(i,f);
        }
    }

    public void deleteAllChildrenOlderThan(int num){
        db.getAllFamilies().stream().forEach(x->{
            ArrayList<Human> collect =(ArrayList<Human>) x.getChildren().stream().filter(y -> y.getFullYear() > num).collect(Collectors.toList());
            x.setChildren(collect);
        });
    }

    public void count(){
        System.out.printf("Count %d",db.getAllFamilies().size());
    }

    public Family getFamilyById(int id){
        return db.getFamilyByIndex(id);
    }

    public Set<Pet> getPets(int index){
        Family f = getFamilyById(index);
        return f.getPet();
    }

    public void addPet(int index, Pet p){
        Family f = getFamilyById(index);
        HashSet<Pet> pets = f.getPet();
        pets.add(p);
        f.setPet(pets);
        int i = db.getAllFamilies().indexOf(f);
        db.getAllFamilies().set(i,f);
    }
}
