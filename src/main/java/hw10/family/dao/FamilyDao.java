package hw10.family.dao;

import hw10.Family;

import java.util.List;


public interface FamilyDao<A> {
    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    boolean deleteFamily(Family f);
    boolean deleteFamily(int index);
    void saveFamily(Family f);

}
