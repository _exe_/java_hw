package hw10;

import hw10.family.controller.FamilyController;

import java.util.HashMap;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        HashMap<String, String> schedule1 = new HashMap<>();
        schedule1.put(DayOfWeek.SUNDAY.name(),"do home work");
        HashMap<String, String> schedule2 = new HashMap<>();
        schedule2.put(DayOfWeek.THURSDAY.name(),"Go to the park");
        HashSet<String> habits1 = new HashSet<>();
        habits1.add("find parents");
        habits1.add("forget");
        Man ch1 = new Man("Evgeniy", "Ponasenkov", "29/09/2000", 200);

        FamilyController db = new FamilyController();
        db.createNewFamily(new Woman("Caroline", "Hill", "29/09/1967", 90, schedule1), new Man("Yoshinobu", "Hill", "01/02/1953", 90,schedule2));
        db.bornChild(db.getFamilyById(0),"Boris", "Polya");
        db.adoptChild(db.getFamilyById(0),ch1);
        db.createNewFamily(new Woman("Nelson", "Barajas", "01/09/1937", 90, schedule1), new Man("Ara ", "Barajas", "05/01/1980", 90,schedule2));
        db.deleteAllChildrenOlderThan(15);
        db.getFamiliesBiggerThan(2);
        db.countFamiliesWithMemberNumber(3);
        db.addPet(0, new RoboCat("DarkKnight88", 10,90,habits1));
        System.out.println("ALL FAMILIES");
        db.displayAllFamilies();
    }
}