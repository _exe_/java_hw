package hw5;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {
//
    public String[][] schedule(){
        String[][] schedule = new String[7][2];
        schedule[0][0] = DayOfWeek.SUNDAY.name();
        schedule[0][1] = "do home work";
        schedule[1][0] =  DayOfWeek.MONDAY.name();
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = DayOfWeek.TUESDAY.name();
        schedule[2][1] = "Learn React";
        schedule[3][0] =  DayOfWeek.WEDNESDAY.name();
        schedule[3][1] = "Learn JS";
        schedule[4][0] =  DayOfWeek.THURSDAY.name();
        schedule[4][1] = "Go to the park";
        schedule[5][0] =  DayOfWeek.FRIDAY.name();
        schedule[5][1] = "Buy a coffee cup";
        schedule[6][0] =  DayOfWeek.SATURDAY.name();
        schedule[6][1] = "Learn japanese";
        return  schedule;
    }

    @Test
    void testToString() {
        Human h1 = new Human("Mary", "Poppins", 60, 90, schedule());
        String expected = "Human{name='Mary', surname='Poppins', year=60, iq=90, schedule=[[SUNDAY, do home work], [MONDAY, go to courses; watch a film], [TUESDAY, Learn React], [WEDNESDAY, Learn JS], [THURSDAY, Go to the park], [FRIDAY, Buy a coffee cup], [SATURDAY, Learn japanese]]}";
        assertEquals(expected, h1.toString());
    }
    @Test
    void testToString1() {
        Human h1 = new Human("Mary", "Poppins", 60);
        String expected = "Human{name='Mary', surname='Poppins', year=60}";
        assertEquals(expected, h1.toString());
    }

}