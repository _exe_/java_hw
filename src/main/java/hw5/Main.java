package hw5;


public class Main {
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];
        schedule[0][0] = DayOfWeek.SUNDAY.name();
        schedule[0][1] = "do home work";
        schedule[1][0] =  DayOfWeek.MONDAY.name();
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = DayOfWeek.TUESDAY.name();
        schedule[2][1] = "Learn React";
        schedule[3][0] =  DayOfWeek.WEDNESDAY.name();
        schedule[3][1] = "Learn JS";
        schedule[4][0] =  DayOfWeek.THURSDAY.name();
        schedule[4][1] = "Go to the park";
        schedule[5][0] =  DayOfWeek.FRIDAY.name();
        schedule[5][1] = "Buy a coffee cup";
        schedule[6][0] =  DayOfWeek.SATURDAY.name();
        schedule[6][1] = "Learn japanese";

        Human[] children = new Human[2];
        children[0] =  new Human("Sergey", "Liskov", 12,200, schedule);
        children[1] =  new Human("Vladimir", "Hammer", 3,120,schedule);
        Family f1 = new Family(new Human("Mary", "Poppins", 60, 90, schedule),
                new Human("Vanya", "Bumpkin", 53, 90, schedule),
                children);
//        System.out.println(f1.toString());


        Human h1 = new Human("Mary", "Poppins", 60);
        System.out.println(h1.toString());
//        for (int i = 0; i < 10_000_00; i++){
//            new Human("Mary", "Poppins", 60, 90, new Pet(Species.BIRD,"cherry",5,50, new String[]{"eat","sleep"}), schedule);
//        }
    }


}
