package hw5;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    public String[][] schedule(){
        String[][] schedule = new String[7][2];
        schedule[0][0] = DayOfWeek.SUNDAY.name();
        schedule[0][1] = "do home work";
        schedule[1][0] =  DayOfWeek.MONDAY.name();
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = DayOfWeek.TUESDAY.name();
        schedule[2][1] = "Learn React";
        schedule[3][0] =  DayOfWeek.WEDNESDAY.name();
        schedule[3][1] = "Learn JS";
        schedule[4][0] =  DayOfWeek.THURSDAY.name();
        schedule[4][1] = "Go to the park";
        schedule[5][0] =  DayOfWeek.FRIDAY.name();
        schedule[5][1] = "Buy a coffee cup";
        schedule[6][0] =  DayOfWeek.SATURDAY.name();
        schedule[6][1] = "Learn japanese";
        return  schedule;
    }


    public Family createFamily(){
        Human[] children = new Human[2];
        children[0] =  new Human("Sergey", "Liskov", 12,200, schedule());
        children[1] =  new Human("Vladimir", "Hammer", 3,120,schedule());
        Family family = new Family(new Human("Mary", "Poppins", 60, 90, schedule()),
                new Human("Vanya", "Bumpkin", 53, 90, schedule()),
                children);
        return  family;
    }

    @Test
    void addChild() {
        Family f1 = createFamily();
        Human[] children = f1.getChildren();
        int totalItems = f1.getTotalItems();
        int newTotalItems = totalItems;
        Human nc =  new Human("Nick", "Kolyma", 25,233330, schedule());

        if (children.length == 0 || totalItems == children.length){
            Human newArr[] = new Human[children.length + 10];
            for (int i = 0; i < children.length; i++) newArr[i] = f1.getChild(i);
            newArr[totalItems++] = nc;
            newTotalItems++;
            f1.setChildren(newArr);
        } else {
            Human newArr[] = new Human[children.length];
            for (int i = 0; i < children.length; i++) newArr[i] = f1.getChild(i);
            newArr[totalItems++] = nc;
            newTotalItems++;
            f1.setChildren(newArr);
        }
        f1.setTotalItems(newTotalItems);



        Human[] realArr = new Human[f1.getTotalItems()];
        for (int i = 0; i < realArr.length; i++) {
            realArr[i] = f1.getChild(i);
        }

        Human[] expectArr = new Human[3];
        expectArr[0] =  new Human("Sergey", "Liskov", 12,200, schedule());
        expectArr[1] =  new Human("Vladimir", "Hammer", 3,120,schedule());
        expectArr[2] = new Human("Nick", "Kolyma", 25,233330, schedule());
        assertEquals(Arrays.toString(expectArr), Arrays.toString(realArr));
    }

    @Test
    void deleteChild() {
        Family f1 = createFamily();
        int item = 1;
        Human[] children = f1.getChildren();
        Human newArr[] = new Human[children.length -1];
        for (int i = 0, j = 0; i < children.length; i++) {
            if (i != item) {
                newArr[j++] = children[i];
            } else {
                children[i].setFamily(null);
            }
        }
        f1.setChildren(newArr);

        Human[] expectArr = new Human[1];
        expectArr[0] = new Human("Sergey", "Liskov", 12,200, schedule());
        assertEquals(Arrays.toString(expectArr), Arrays.toString(newArr));
    }
    @Test
    void count(){
        Family f1 = createFamily();

        int real = 4;
        int expect = f1.count();
        assertEquals(expect,real);
    }
}