package hw12.exceptions;

public class FamilyOverflowException  extends  RuntimeException{
    public FamilyOverflowException(String errorMessage) {
        super(errorMessage);
    }

}
