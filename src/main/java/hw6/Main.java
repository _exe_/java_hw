package hw6;

public class Main {
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];
        schedule[0][0] = DayOfWeek.SUNDAY.name();
        schedule[0][1] = "do home work";
        schedule[1][0] =  DayOfWeek.MONDAY.name();
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = DayOfWeek.TUESDAY.name();
        schedule[2][1] = "Learn React";
        schedule[3][0] =  DayOfWeek.WEDNESDAY.name();
        schedule[3][1] = "Learn JS";
        schedule[4][0] =  DayOfWeek.THURSDAY.name();
        schedule[4][1] = "Go to the park";
        schedule[5][0] =  DayOfWeek.FRIDAY.name();
        schedule[5][1] = "Buy a coffee cup";
        schedule[6][0] =  DayOfWeek.SATURDAY.name();
        schedule[6][1] = "Learn japanese";

        Human[] children = new Human[2];
        children[0] =  new Man("Sergey", "Liskov", 10,120, schedule);
        children[1] =  new Woman("Elena", "Liskova", 3,60, schedule);
        Family f1 = new Family(new Woman("Mary", "Poppins", 60, 90, schedule), new Man("Vanya", "Bumpkin", 53, 90, schedule), children, new Fish("dory",10,99,new String[]{"find parents","forget"}));

        Human[] children1 = new Human[2];
        children1[0] =  new Man("Sergey", "Liskov", 10,120, schedule);
        children1[1] =  new Woman("Elena", "Liskova", 3,60, schedule);
        Family f2 = new Family(
                new Woman("Mary", "Poppins", 60, 90, schedule),
                new Man("Vanya", "Bumpkin", 53, 90, schedule),
                children,
                new RoboCat("cyber murka",20,100,new String[]{"crash","oil"}));

        Family f3 = new Family(new Woman("Mary", "Poppins", 60, 90, schedule), new Man("Vanya", "Bumpkin", 53, 90, schedule), new Dog("mukhtar",5,10,new String[]{"eat meat"}));


        System.out.println(f1.toString());
        System.out.println("===============");
        System.out.println(f2.toString());
        System.out.println("===============");
        System.out.println(f3.toString());



    }
}
