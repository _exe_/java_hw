package hw6;

public enum Species {
    DOMESTIC_CAT,
    ROBO_CAT,
    FISH,
    DOG,
    UNKNOWN
}
