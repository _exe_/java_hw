package hw6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

public class Family {
    private int totalItems = 0;
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Woman mother, Man father) {
        mother.setFamily(this);
        father.setFamily(this);
        this.mother = mother;
        this.father = father;
        this.children = new Human[0];
        this.totalItems = children.length;
    }

    public Family(Woman mother, Man father, Human[] children, Pet pet) {
        mother.setFamily(this);
        father.setFamily(this);
        mother.setPet(pet);
        father.setPet(pet);
        this.mother = mother;
        this.father = father;
        this.totalItems = children.length;
        this.children = children;
        this.pet = pet;
        for (Human p:children) {
            p.setPet(pet);
        }
    }

    public Family(Woman mother, Man father, Pet pet) {
        mother.setFamily(this);
        father.setFamily(this);
        mother.setPet(pet);
        father.setPet(pet);
        this.mother = mother;
        this.father = father;
        this.children = new Human[0];
        this.totalItems = children.length;
        this.pet = pet;
    }


    public void addChild(Human c){
        c.setFamily(this);
        if (children.length == 0 || totalItems == children.length){
        Human newArr[] = new Human[children.length + 10];
            for (int i = 0; i < children.length; i++) newArr[i] = getChild(i);
            newArr[totalItems++] = c;
            children = newArr;
        } else {
            Human newArr[] = new Human[children.length];
            for (int i = 0; i < children.length; i++) newArr[i] = getChild(i);
            newArr[totalItems++] = c;
            children = newArr;
        }
    }

    public Human getChild(int index) {
        return children[index];
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public void deleteChild(int index){
        Human newArr[] = new Human[children.length -1];
        for (int i = 0, j = 0; i < children.length; i++) {
            if (i != index) {
                newArr[j++] = children[i];
            } else {
                children[i].setFamily(null);
            }
        }
        children = newArr;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    protected int count(){
        return 2 + totalItems;
    }

    public Pet getPet() {
        return pet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family = (Family) o;
        return totalItems == family.totalItems && Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Arrays.equals(children, family.children) && pet.equals(family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(totalItems, mother, father);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    public Human getMother() {
        return mother;
    }

    @Override
    public String toString() {
        if (children.length == 0){
            return String.format("mother=%s, \n father=%s,\n", mother, father);
        } else {
        return String.format("mother=%s, \n father=%s, \n children=%s,\n", mother, father, Arrays.toString(children));

        }
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(toString());
        super.finalize();
    }
}
