package hw6;


import java.util.Arrays;
import java.util.Objects;

public abstract class Human {
    StringBuilder sb = new StringBuilder("");

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private Pet pet;
    private String[][] schedule;

    public Human(){};

    public Human(String name, String surname, int year){
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, String[][] schedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int year, int iq, Pet pet, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.schedule = schedule;
    }

    private String formatSchedule(){
            for(int row = 0; row< schedule.length; row++){
                if (row == 0){
                    sb.append("[");
                }
                for (int col = 0; col< schedule[row].length; col++){
                    if (col == 0){
                        sb.append("[");
                    }
                    sb.append(schedule[row][col]);
                    if (col == schedule[row].length -1){
                        sb.append("]");
                    } else {
                        sb.append(", ");
                    }
                }

                if (row == schedule.length - 1){
                    sb.append("]");
                } else {
                    sb.append(", ");
                }
            }
            return sb.toString();

    }

    public abstract String greetPet();

    public String describePet(){
        return String.format("У меня есть %s, ему %d лет, он %s", pet.getNickname(), pet.getAge(), pet.describeTricky());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Pet getPet() {
        return pet;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public String toString() {
        if (schedule == null && iq == 0){
            return String.format("Human{name='%s', surname='%s', year=%d}", name, surname, year);
        } else if (pet != null){
            return String.format("Human{name='%s', surname='%s', year=%d, iq=%d, %s schedule=%s}", name, surname, year, iq,pet.toString(), formatSchedule());
        } else {
            return String.format("Human{name='%s', surname='%s', year=%d, iq=%d schedule=%s}", name, surname, year, iq, formatSchedule());

        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return year == human.year
                && iq == human.iq
                && Objects.equals(name, human.name)
                && Objects.equals(surname, human.surname)
                && Objects.equals(family, human.family)
                && pet.equals(human.pet)
                && Arrays.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(toString());
        super.finalize();
    }
}
