package hw6;

public final class Man extends Human{

    public Man(String name, String surname, int year){
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule){
        super(name, surname, year, iq, schedule);
    }

    public Man(String name, String surname, int year, int iq, Pet pet, String[][] schedule) {
        super(name,surname,year,iq,pet,schedule);
    }

    @Override
    public String greetPet() {
        return String.format("%s",getPet().getNickname());
    }

    public void repairCar(){
        System.out.printf("%s is starting to repair the car", super.getName());
    }

}
