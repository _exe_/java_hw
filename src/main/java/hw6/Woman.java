package hw6;

public class Woman  extends Human{
    public Woman(String name, String surname, int year){
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule){
        super(name, surname, year, iq, schedule);
    }

    public Woman(String name, String surname, int year, int iq, Pet pet, String[][] schedule) {
        super(name,surname,year,iq,pet,schedule);
    }

    @Override
    public String greetPet() {
        return String.format("%s",getPet().getNickname());
    }

    public void makeUp(){
        System.out.printf("%s starts to makeup", super.getName());
    }
}
