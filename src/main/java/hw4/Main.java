package hw4;


public class Main {
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "Learn React";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "Learn JS";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "Go to the park";
        schedule[5][0] = "Friday";
        schedule[5][1] = "Buy a coffee cup";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "Learn japanese";

        Human[] children = new Human[2];
        children[0] =  new Human("Sergey", "Liskov", 12,200, schedule);
        children[1] =  new Human("Vladimir", "Hammer", 3,120,schedule);
        Family f1 = new Family(new Human("Mary", "Poppins", 60, 90, schedule),
                new Human("Vanya", "Bumpkin", 53, 90, schedule),
                children);

        f1.addChild(new Human("FASFSAASFAF", "GQGQGQQGE", 53, 90, schedule));
        Family f2 = new Family(
                new Human("Mary", "Poppins", 60, 90, new Pet("dog","cherry",5,50, new String[]{"eat","sleep"}), schedule),
                new Human("Vanya", "Bumpkin", 53, 90, new Pet("cat",
                        "abra",
                        5,
                        100,
                        new String[]{"eat","sleep"}), schedule)
               );
        f1.addChild(new Human("Andrew", "Howard",19,90,schedule));
        System.out.println(f1.toString());
        System.out.println(f2.toString());
        System.out.println(f2.getMother().describePet());
    }


}
