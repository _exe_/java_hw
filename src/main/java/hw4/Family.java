package hw4;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private int totalItems = 0;
    private Human mother;
    private Human father;
    private Human[] children;

    public Family(Human mother, Human father) {
        mother.setFamily(this);
        father.setFamily(this);
        this.mother = mother;
        this.father = father;
        this.children = new Human[0];
        this.totalItems = children.length;
    }
    public Family(Human mother, Human father, Human[] children) {
        mother.setFamily(this);
        father.setFamily(this);
        this.mother = mother;
        this.father = father;
        this.totalItems = children.length;
        this.children = children;
    }

    public void addChild(Human c){
        c.setFamily(this);
        if (children.length == 0 || totalItems == children.length){
        Human newArr[] = new Human[children.length + 10];
            for (int i = 0; i < children.length; i++) newArr[i] = getChild(i);
            newArr[totalItems++] = c;
            children = newArr;
        } else {
            Human newArr[] = new Human[children.length];
            for (int i = 0; i < children.length; i++) newArr[i] = getChild(i);
            newArr[totalItems++] = c;
            children = newArr;
        }
    }

    public Human getChild(int index) {
        return children[index];
    }


    public void deleteChild(int index){
        Human newArr[] = new Human[children.length -1];
        for (int i = 0, j = 0; i < children.length; i++) {
            if (i != index) {
                newArr[j++] = children[i];
            } else {
                children[i].setFamily(null);
            }
        }
        children = newArr;
    }

    public int getTotalItems() {
        return totalItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return totalItems == family.totalItems && Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Arrays.equals(children, family.children);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(totalItems, mother, father);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    public Human getMother() {
        return mother;
    }

    @Override
    public String toString() {
        return String.format("mother=%s, \n father=%s, \n children=%s,\n", mother, father, Arrays.toString(children));
    }
}
