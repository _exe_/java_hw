package hw9.family.dao;

import hw9.Family;

import java.util.List;


public interface FamilyDao<A> {
    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    boolean deleteFamily(Family f);
    boolean deleteFamily(int index);
    void saveFamily(Family f);

}
