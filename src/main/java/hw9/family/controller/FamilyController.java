package hw9.family.controller;

import hw9.*;
import hw9.family.service.FamilyService;

import java.util.List;
import java.util.Set;

public class FamilyController {
    private FamilyService familyService = new FamilyService();

    public List<Family> geAllFamilies(){
        return familyService.geAllFamilies();
    }

    public void displayAllFamilies(){
       familyService.displayAllFamilies();
    }

   public void getFamiliesBiggerThan(int c){
       System.out.println("Families Bigger Than");
       familyService.getFamiliesBiggerThan(c);
    }

   public void getFamiliesLessThan(int c){
       System.out.println("Families less Than");
      familyService.getFamiliesLessThan(c);
    }

   public void countFamiliesWithMemberNumber(int c){
       familyService.countFamiliesWithMemberNumber(c);
    }


    public void deleteFamilyByIndex(int index){
       familyService.deleteFamilyByIndex(index);
    }

    public void bornChild(Family f, String maleName, String femaleName){
        familyService.bornChild(f,maleName, femaleName);
    }

    public void adoptChild(Family f, Human child){
        familyService.adoptChild(f,child);
    }
    public void deleteAllChildrenOlderThan(int num){
        familyService.deleteAllChildrenOlderThan(num);
    }

    public void count(){
        familyService.count();
    }

    public Family getFamilyById(int id){
        return familyService.getFamilyById(id);
    }

    public Set<Pet> getPets(int index){
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet p){
        familyService.addPet(index,p);
    }

    public void createNewFamily(Woman mother, Man father){
        familyService.createNewFamily(mother, father);
    }

}
