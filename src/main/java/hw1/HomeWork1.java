package hw1;

import java.util.Arrays;
import java.util.Scanner;

public class HomeWork1 {
    static Scanner sc = new Scanner(System.in);
    static int countNumbers = 0;
    static int[] userNumList = new int[100];
    public static void main(String[] args) {
        start();
    }

    public static int randomInteger(){
        int randomNum = (int) ((Math.random()*100));
        return randomNum;
    }

    public static int expectInt(Scanner sc) {
        do {
            String input = sc.next();
            if (isInt(input)){
                return toInt(input);
            } else {
                System.out.printf("You have not typed an number %s", input);
            }
        }while (true);
    }

    public static int toInt(String input) {
        return Integer.parseInt(input);
    }

    public static boolean isInt(String input) {
        try {
            int value = toInt(input);
            return true;
        } catch (NumberFormatException x) {
            return false;
        }
    }

    public static int userNumber(){
        int userNumber = expectInt(sc);
        return userNumber;
    }

    public static String userName(){
        String name = sc.next();
        return name;
    }

    public static void start(){
        System.out.println("Let the game begin!");
        int randomInteger = randomInteger();
        System.out.printf("Random integer %d \n",randomInteger);
        System.out.println("Your name");
        String name = userName();
        game(randomInteger, name);
    }


    public static void addElem(int num){
        if (countNumbers == userNumList.length - 1){
            addElem(num);
        } else {
            userNumList[countNumbers] = num;
            countNumbers++;
        }
    }

    public static void game(int randomInteger, String name){
        System.out.printf("Your name %s\n", name);
        System.out.println("Write a number");
        while (true){
            int un = userNumber();
            if (un == randomInteger){
                sc.close();
                break;
            } else if(un < randomInteger){
                System.out.println("Your number is too small. Please, try again.");
                addElem(un);
            } else {
                System.out.println("Your number is too big. Please, try again.");
                addElem(un);
            }
        };
        end(name, userNumList, countNumbers);
    }

    public static void end(String name, int[] userNumList, int countNumbers) {
        System.out.printf("Congratulations, %s \n", name);
        int[] showNumbers = Arrays.copyOf(userNumList, countNumbers);
        System.out.printf("Your numbers: %s \n", Arrays.toString(showNumbers));
    }
}
