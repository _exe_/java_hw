package hw7;

import java.util.HashMap;
import java.util.HashSet;

public final class Man extends Human {

    public Man(String name, String surname, int year){
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, HashMap<String, String> schedule){
        super(name, surname, year, iq, schedule);
    }

    public Man(String name, String surname, int year, int iq, HashSet<Pet> pet, HashMap<String, String> schedule) {
        super(name,surname,year,iq,pet,schedule);
    }

    @Override
    public String greetPet() {
        return String.format("%s",this.getPet());
    }

    public void repairCar(){
        System.out.printf("%s is starting to repair the car", super.getName());
    }

}
