package hw7;


import java.util.*;

public abstract class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private HashSet<Pet> pet;
    private HashMap<String, String> schedule;

    public Human(){};

    public Human(String name, String surname, int year){
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, HashMap<String, String> schedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int year, int iq, HashSet<Pet> pet, HashMap<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.schedule = schedule;
    }

    private String formatSchedule(){
        StringBuilder sb = new StringBuilder("");

        if (schedule.size() != 0){
                 sb.append("[");
                for (String key : schedule.keySet()){
                    sb.append("[");
                    sb.append(key + " ");
                    sb.append(schedule.get(key));
                    sb.append("]");
                }
                sb.append("]");
             }
            return sb.toString();
    }

    public abstract String greetPet();

    public String describePet(){
        StringBuilder sb = new StringBuilder("");

        HashSet<Pet> pets = getFamily().getPet();
        Iterator itr = pets.iterator();
        while (itr.hasNext()){
            Pet p = (Pet) itr.next();
            sb.append("У меня есть ");
            sb.append(p.getNickname());
            sb.append(" ему ");
            sb.append(p.getAge());
            sb.append(" лет, он ");
            sb.append(p.describeTricky());
        }
        return sb.toString();
    }
    public String formatPets(){
        StringBuilder sb = new StringBuilder("");

        HashSet<Pet> pets = family.getPet();
        Iterator itr = pets.iterator();
        while (itr.hasNext()){
            Pet p = (Pet) itr.next();
            sb.append("[");
            sb.append(p.toString());
            sb.append("]");
        }
        return sb.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setPet(HashSet<Pet> pet) {
        this.pet = pet;
    }

    public HashSet<Pet> getPet() {
        return pet;
    }

    public HashMap<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(HashMap<String, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public String toString() {
        if (schedule == null && iq == 0) {
            return String.format("Human{name='%s', surname='%s', year=%d}", name, surname, year);
        } else {
            return String.format("Human{name='%s', surname='%s', year=%d, iq=%d, pet=%s schedule=%s}", name, surname, year, iq, formatPets(), formatSchedule());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return year == human.year && iq == human.iq && name.equals(human.name) && surname.equals(human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq, family);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(toString());
        super.finalize();
    }
}
