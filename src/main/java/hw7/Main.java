package hw7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        HashMap<String, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.SUNDAY.name(),"do home work");
        schedule.put(DayOfWeek.MONDAY.name(),"go to courses; watch a film");
        schedule.put(DayOfWeek.TUESDAY.name(),"Learn React");
        schedule.put(DayOfWeek.WEDNESDAY.name(),"Learn JS");
        schedule.put(DayOfWeek.THURSDAY.name(),"Go to the park");
        schedule.put(DayOfWeek.FRIDAY.name(),"Buy a coffee cup");
        schedule.put(DayOfWeek.SATURDAY.name(),"Learn japanese");

        HashSet<String> habits = new HashSet<>();
        habits.add("eat meat");
        HashSet<String> habits1 = new HashSet<>();
        habits.add("find parents");
        habits.add("forget");


        HashSet<Pet> pets = new HashSet<>();
        pets.add(new Dog("mukhtar",5,10, habits));
        pets.add(new Fish("dory",10,99,habits1));

        ArrayList<Human> children = new ArrayList<>();
        children.add(new Man("Sergey", "Liskov", 10,120, schedule));
        children.add(new Woman("Elena", "Liskova", 3,60, schedule));

        Family f1 = new Family(new Woman("Yoneko", "Patterson", 60, 90, schedule),
                new Man("Vanya", "Patterson", 53, 90, schedule),
                children,
                pets);

        Family f2 = new Family(
                new Woman("Zayna", "Griffin", 60, 90, schedule),
                new Man("Nelson", "Griffin", 53, 90, schedule),
                children,
                pets);

        Family f3 = new Family(new Woman("Caroline", "Hill", 60, 90, schedule),
                new Man("Yoshinobu", "Hill", 53, 90, schedule),
                pets);

        f1.deleteChild(new Man("Sergey", "Liskov", 10,120, schedule));

        System.out.println("===============");
        System.out.println(f1.toString());
        System.out.println("===============");
        System.out.println(f2.toString());
        f3.addChild(new Man("Vanya", "Hill", 53, 90, schedule));
        System.out.println(f3.toString());
    }
}