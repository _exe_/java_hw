package hw11.exceptions;

public class FamilyOverflowException  extends  RuntimeException{
    public FamilyOverflowException(String errorMessage) {
        super(errorMessage);
    }

}
