package hw11.family.dao;

import hw11.Family;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;


public interface FamilyDao<A> {
    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    boolean deleteFamily(Family f);
    boolean deleteFamily(int index);
    void saveFamily(Family f);

}
