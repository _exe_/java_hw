package hw3;

import java.util.Scanner;
import java.util.StringJoiner;


public class HomeWork3 {
    static String[][] schedule = new String[7][2];
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        start();
    }

    public static void start() {
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "Learn React";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "Learn JS";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "Go to the park";
        schedule[5][0] = "Friday";
        schedule[5][1] = "Buy a coffee cup";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "Learn japanese";
        program();
    }

    public static void program(){
        while (true){
            System.out.println("Please, input the day of the week:");
            String a = ask();
            switch (DayOfWeek.valueOf(a)){
                case sunday:
                    print(schedule[0]);
                    break;
                case monday:
                    print(schedule[1]);
                    break;
                case tuesday:
                    print(schedule[2]);
                    break;
                case wednesday:
                    print(schedule[3]);
                    break;
                case thursday:
                    print(schedule[4]);
                    break;
                case friday:
                    print(schedule[5]);
                    break;
                case saturday:
                    print(schedule[6]);
                    break;
                case exit:
                    System.exit(0);
            }
        }
    }

    private static void print(String[] si) {
        StringJoiner output = new StringJoiner(";");
        for (int i = 1; i<si.length; i++){
            output.add(si[i]);
        }
        System.out.printf("Your tasks for %s: %s\n", si[0], output);
    }

    public static boolean contains(String s){
        boolean cont = false;
        String[] words = s.split(" ");
        if (words.length > 1) cont = false;
        for (DayOfWeek d : DayOfWeek.values()) {
            if (d.name().equals(s)) cont = true;
        }
        return cont;
    }

    public static String ask() {
        do {
            String input = sc.nextLine().toLowerCase().trim();
            if (contains(input)){
               return input;
            } else {
                System.out.println("Sorry, I don't understand you, please try again");
            }
        } while (true);
    }
}
