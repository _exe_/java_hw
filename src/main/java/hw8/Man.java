package hw8;

import java.util.HashMap;
import java.util.HashSet;

public final class Man extends Human {


    public Man(String name, String surname, int year){
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, HashMap<String, String> schedule){
        super(name, surname, year, iq, schedule);
    }

    public Man(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    @Override
    public String greetPet() {
        return String.format("%s", getFamily().getPet());
    }

    public void repairCar(){
        System.out.printf("%s is starting to repair the car", super.getName());
    }

}
