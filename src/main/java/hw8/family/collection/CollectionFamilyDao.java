package hw8.family.collection;

import hw8.*;
import hw8.family.dao.FamilyDao;

import java.util.*;

public class CollectionFamilyDao<A> implements FamilyDao<A> {
    private final List<Family> db = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        if (!(db.isEmpty())){
            return db;
        } else {
            return null;
        }
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index > db.size() - 1){
            return null;
        } else {
            return  db.get(index);
        }
    }

    @Override
    public boolean deleteFamily(Family f) {
        if (!db.contains(f)){
            return false;
        } else {
            db.remove(f);
            return true;
        }
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index > db.size() - 1){
            return false;
        } else {
            db.remove(index);
            return true;
        }
    }

    @Override
    public void saveFamily(Family f) {
        if (db.contains(f)){
            int i = db.indexOf(f);
            db.set(i,f);
        } else {
            db.add(f);
        }
    }
}
