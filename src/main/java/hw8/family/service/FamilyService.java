package hw8.family.service;


import hw8.*;
import hw8.family.collection.CollectionFamilyDao;
import hw8.family.dao.FamilyDao;

import java.util.*;

public class FamilyService {
    private FamilyDao<Family> db  = new CollectionFamilyDao<>();

    public List<Family> geAllFamilies(){
        return db.getAllFamilies();
    }

    public void displayAllFamilies(){
        List<Family> allFamilies = db.getAllFamilies();
        for (int i = 0; i < allFamilies.size(); i++) {
            System.out.printf("Index: %d Family: %s",i,allFamilies.get(i));
        }
    }

    public void displayAllFamilies(List<Family> families) {
        for (int i = 0; i < families.size(); i++) {
            System.out.printf("Index: %d Family: %s",i,families.get(i));
        }
    }

    public void getFamiliesBiggerThan(int c){
        List<Family> sortedFamilies =  new ArrayList<>();
        for (Family family:db.getAllFamilies()){
            if (family.count() > c){
                sortedFamilies.add(family);
            }
        }
         displayAllFamilies(sortedFamilies);
    }

    public void getFamiliesLessThan(int c){
        List<Family> sortedFamilies =  new ArrayList<>();
        for (Family family:db.getAllFamilies()){
            if (family.count() < c){
                sortedFamilies.add(family);
            }
        }
        displayAllFamilies(sortedFamilies);
    }

    public void countFamiliesWithMemberNumber(int c){
        int count = 0;
        for (Family family:db.getAllFamilies()){
            if (family.count() == c){
                count +=1;
            }
        }
        System.out.println(count);
    }

    public void createNewFamily(Woman mother, Man father){
        Family family = new Family(mother, father);
        db.saveFamily(family);
    }

    public void deleteFamilyByIndex(int index){
        System.out.println(db.deleteFamily(index));
    }

    public void bornChild(Family f, String maleName, String femaleName){
        if (db.getAllFamilies().contains(f)){
            int randomGender = (int) (Math.random()*1);
            Human ch;
            if (randomGender == 1){
                ch = new Woman(femaleName, f.getMother().getSurname(), 0);
            } else {
                ch = new Man(maleName, f.getMother().getSurname(), 0);
            }
            f.addChild(ch);
            int i = db.getAllFamilies().indexOf(f);
            db.getAllFamilies().set(i,f);
        }
    }

    public void adoptChild(Family f, Human child){
        if (db.getAllFamilies().contains(f)){
            f.addChild(child);
            int i = db.getAllFamilies().indexOf(f);
            db.getAllFamilies().set(i,f);
        }
    }

    public void deleteAllChildrenOlderThan(int num){
        for (Family family: db.getAllFamilies()){
            Iterator<Human> it = family.getChildren().iterator();
            while (it.hasNext()){
                Human child = it.next();
                if (num < child.getYear()){
                    it.remove();
                    int i = db.getAllFamilies().indexOf(family);
                    db.getAllFamilies().set(i,family);
                }
            }
        }
    }

    public void count(){
        System.out.printf("Count %d",db.getAllFamilies().size());
    }

    public Family getFamilyById(int id){
        return db.getFamilyByIndex(id);
    }

    public Set<Pet> getPets(int index){
        Family f = getFamilyById(index);
        return f.getPet();
    }

    public void addPet(int index, Pet p){
        Family f = getFamilyById(index);
        HashSet<Pet> pets = f.getPet();
        pets.add(p);
        f.setPet(pets);
        int i = db.getAllFamilies().indexOf(f);
        db.getAllFamilies().set(i,f);
    }
}
