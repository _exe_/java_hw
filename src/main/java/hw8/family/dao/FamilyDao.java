package hw8.family.dao;

import hw8.Family;
import hw8.Human;
import hw8.Pet;

import java.util.List;
import java.util.Set;


public interface FamilyDao<A> {
    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    boolean deleteFamily(Family f);
    boolean deleteFamily(int index);
    void saveFamily(Family f);

}
