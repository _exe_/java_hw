package hw8;

import java.util.HashMap;
import java.util.HashSet;

public class Woman  extends Human {

    public Woman(String name, String surname, int year){
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, HashMap<String, String> schedule){
        super(name, surname, year, iq, schedule);
    }


    @Override
    public String greetPet() {
        return String.format("%s",getFamily().getPet());
    }

    public void makeUp(){
        System.out.printf("%s starts to makeup", super.getName());
    }


}
